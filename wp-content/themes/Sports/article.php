<section class="area">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<ul class="visual-list">
						<li>
							<div class="img-holder">
								<img src="wp-content/themes/Sports/images/graph-04.svg" width="110" alt="">
							</div>
							<div class="text-holder">
								<?php wp_title()   ?>
								<?php wp_content() ?>
							</div>
						</li>
				<div class="col-md-7">
					<div class="slide-holder">
						<?php the_post_thumbnail() ?>
					</div>
				</div>
			</div>
		</div>
</section>