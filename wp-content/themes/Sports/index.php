<?php get_header(); 
	include 'functions.php' ?>
<body>
<div id="wrapper">
	<section class="visual">
		<div class="container">
			<div class="text-block">
				<div class="heading-holder">
					<h1>Design your sports</h1>
				</div>
				<p class="tagline">A happier & healthier life is waiting for you!</p>
				<span class="info">Get motivated now</span>
			</div>
		</div>
		<img src="wp-content/themes/Sports/images/img-bg-01.jpg" alt="" class="bg-stretch">
	</section>
	<section class="main">
		<div class="container">
			<div id="cta">
				<a href="javascript:" class="btn btn-primary rounded">Start Training Now</a>
				<p>Every day is a challenge, change your life now.</p>
			</div>
			<div class="row">
				<div class="text-box col-md-offset-1 col-md-10">
					<h2>About the Club</h2>
					<p>An athlete cannot run with money in his pockets. He must run with hope in his heart and dreams in his head.</p>
				</div>
			</div>
		</div>
	</section>
	<?php 
		if ( have_posts() ) : 
			
			while ( have_posts() ) : the_post();
			
				the_content('article.php');
				
			endwhile;
		else :
			_e( 'Sorry, no posts matched your criteria.', 'textdomain' );
		endif;
	?>
	
	<section class="visual-container">
		<div class="visual-area">
			<div class="container">
				<h2>What Our Clients Say</h2>
				<ul class="testimonials">
					<li>
						<div class="img-holder"><a href="#"><img src="wp-content/themes/Sports/images/logo-1.png" height="84" width="124" alt=""></a></div>
						<p><em>Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim magna. Sed varius urna et pulvinar venenatis. </em></p>
					</li>
				</ul>
			</div>
		</div>
		<div class="visual-area">
			<div class="container">
				<h2>Pricing Plans</h2>
				<div class="pricing-tables">
					<div class="plan">
		                <div class="head">
		                    <h3>Students</h3>
		                </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">$</span>8</span>
		                    <span class="price-additional">per month</span>
		                </div>
		                <ul class="item-list">
		                    <li>Personal License</li>
		                </ul>
		                <button type="button" class="btn btn-default rounded">purchase</button>
		            </div>
		            <div class="plan">
		                <div class="head">
		                    <h3>professional</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">$</span>19</span>
		                    <span class="price-additional">per month</span>
		                </div>
		                    <ul class="item-list">
		                       <li>Professional License</li>
		                       <li>Email Support</li>
		                    </ul>
		                <button type="button" class="btn btn-default rounded">purchase</button>
		            </div>
		            <div class="plan recommended">
		                <div class="head">
		                    <h3>agency</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">$</span>49</span>
		                    <span class="price-additional">per month</span>
		                </div>
		                    <ul class="item-list">
		                        <li>1-12 Team Members</li>
		                        <li>Phone Support</li>
		                    </ul>
		                <button type="button" class="btn btn-default rounded">purchase</button>
		            </div>
		            <div class="plan">
		                <div class="head">
		                    <h3>enterprise</h3> </div>
		                <div class="price">
		                    <span class="price-main"><span class="symbol">$</span>79</span>
		                    <span class="price-additional">per month</span>
		                </div>
		                <ul class="item-list">
		                    <li>Unlimited Team Members</li>
		                    <li>24/ 7 Phone Support</li>
		                </ul>
		                <button type="button" class="btn btn-default rounded">purchase</button>
		            </div>
				</div>
 			</div>
		</div>
	</section>
    
    
    <section class="main-zero-padd">
    <img src="wp-content/themes/Sports/images/video-placeholder.jpg" alt="" style="max-width:100%;" />
		





	</section>
    
	<section class="area">
		<div class="container">
			<div class="subscribe">
				<h3>Subscribe to Our Newsletter</h3>
				<form class="form-inline">
					<button type="submit" class="btn btn-primary rounded">Subscribe</button>
					<div class="form-group">
						<input type="email" class="form-control rounded" id="exampleInputEmail2" placeholder="Email...">
					</div>
				</form>
			</div>
		</div>
	</section>

	<?php get_footer(); ?>
	
</div>
<script src="wp-content/themes/Sports/js/jquery-1.11.2.min.js"></script>
<script src="wp-content/themes/Sports/js/bootstrap.js"></script>
<script src="wp-content/themes/Sports/js/jquery.main.js"></script>
</body>
</html>