<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Responsive Bootstrap HTML Template - Sports</title>
	<meta name="description" content="A free responsive website template made exclusively for Frittt by Themesforce and Sarfraz Shaukat">
	<meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
	<meta name="author" content="Themesforce and Sarfraz Shaukat for Frittt">
	<link rel="icon" type="wp-content/themes/Sports/favicons/favicon-16x16.png" href="wp-content/themes/Sports/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="stylesheet" href="wp-content/themes/Sports/css/bootstrap.css">
	<link rel="stylesheet" href="wp-content/themes/Sports/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="wp-content/themes/Sports/css/all.css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
</head>
<header id="header">
			<div class="container">
				<div class="logo"><a href="#"><img src="wp-content/themes/Sports/images/logo.png" alt="Sports"></a></div>
				<nav id="nav">
					<div class="opener-holder">
						<a href="#" class="nav-opener"><span></span></a>
					</div>
					<a href="javascript:" class="btn btn-primary rounded">Enroll Today</a>
					<div class="nav-drop">
						<ul>
							<li class="active visible-sm visible-xs"><a href="#">Home</a></li>
							<li><a href="#">Company</a></li>
							<li><a href="#">About Sports</a></li>
							<li><a href="#">Subscription</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
						<div class="drop-holder visible-sm visible-xs">
							<span>Follow Us</span>
							<ul class="social-networks">
								<li><a class="fa fa-github" href="#"></a></li>
								<li><a class="fa fa-twitter" href="#"></a></li>
								<li><a class="fa fa-facebook" href="#"></a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>